%{
#include <stdio.h>
void yyerror(const char* msg) {
    fprintf(stderr, "%s\n", msg);
}
int yylex();
%}
%token CMD_SET
%token CMD_WOUTP
%token CMD1P
%token CMD2P
%token CMD3P
%token SEMICOLON
%token KEY
%token OP
%token COMA
%token ID
%token OTRO

%start LINE

%%
LINE:	|
	    REGLA LINE
      ;
VALUES: ID
        | ID COMA VALUES
;

REGLA: 	CMD_WOUTP SEMICOLON                      {printf("Token sin parametros (Sets, ShowSets)");}
        | CMD1P ID SEMICOLON                     {printf("Token con 1 parametro (ShowSet)");}
	| CMD2P ID COMA ID SEMICOLON             {printf("Token con 2 parametros (Union, Intersect)");}
        | CMD3P ID COMA ID COMA ID SEMICOLON     {printf("Token con 3 parametros (SetUnion, SetIntersect)");}
        | CMD_SET ID OP KEY VALUES KEY SEMICOLON {printf("Token Set");}
;
%%
