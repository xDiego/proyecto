%option noyywrap
%{
    #include "header.h"
    #include "Grammar.tab.h"
%}
%%
Set                     { processToken(CMD_SET, yytext); return CMD_SET;}
Sets                    { processToken(CMD_WOUTP, yytext); return CMD_WOUTP;}
ShowSet                 { processToken(CMD1P, yytext); return CMD1P;}
ShowSets                { processToken(CMD_WOUTP, yytext); return CMD_WOUTP;}
Union                   { processToken(CMD2P, yytext); return CMD2P;}
Intersect               { processToken(CMD2P, yytext); return CMD2P;}
SetUnion                { processToken(CMD3P, yytext); return CMD3P;}
SetIntersect            { processToken(CMD3P, yytext); return CMD3P;}
[a-zA-Z0-9]+            { processToken(ID, yytext); return ID;}
;                       { processToken(SEMICOLON, yytext); return SEMICOLON;}
:=                      { processToken(OP, yytext); return OP;}
\,                       { processToken(COMA, yytext); return COMA;}
\{                       { processToken(KEY, yytext); return KEY;}
\}                       { processToken(KEY, yytext); return KEY;}
.                       { }
%%
