#pragma once

#ifdef __cplusplus
    extern "C" void processToken(int token, char* text);
#else
    void processToken(int token, char* text);
#endif
