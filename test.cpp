#include <iostream>
#include "header.h"

using namespace std;

extern int yylex();
extern int yyparse();

extern "C" void processToken(int token, char* text){
    cout<<"Token("<<token<<","<<text<<")"<<endl;
}

extern "C" int yyerror(char* s){
  cout<< "falló"<<endl;
  return -1;
}

int main(){
    yyparse();
    return 1;
}
